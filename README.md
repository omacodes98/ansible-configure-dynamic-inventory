# Ansible Configiure Dynamic Inventory 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create EC2 instance with Terraform 

* Write Ansible AWS EC2 Plugin to dynamically sets inventory of EC2 servers that Ansible should manage, instead of hard-coding server addresses in Ansible inventoryu file 

## Technologies Used

* Ansible 

* Terraform 

* AWS 

## Steps 

Step 1: Use Terraform to create three cloud servers 

[terraform config file](/images/01_use_terraform_to_create_three_cloud_servers.png)

[terraform apply success](/images/02_terraform_apply_completed.png)

[instances running on console](/images/03_three_instances_running_on_console.png)

Step 2: Make sure you have python , boto3 and botocore library installed 

[Requirments](/images/04_make_sure_you_have_python_installed_boto3_and_botocore_library_installed.png)

Step 3: Go to ansible.cfg  and enable plugin by setting enable_plugins to aws_ec2

[Enabling plugins](/images/05_go_to_ansible_config_and_enable_plugin_by_setting_enable_plugins_to_aws_ec2.png)

Step 4: Create plugins configuration file its important that the last part of the naming of this file has to be aws_ec2

[Created plugins config file](/images/06_create_plugin_configuration_file_its_important_that_the_last_part_of_the_naming_of_this_file_has_to_be_aws_ec2.png)

Step 5: In the plugin config file insert name of plugin

[Name of plugin](/images/07_in_the_plugin_config_file_insert_name_of_plugin.png)

Step 6: Insert region you want access to in plugin config file

[region](/images/08_insert_regions_you_want_access_to.png)

Step 7: Test plugin using ansible inventory command 

    ansible-inventory -i inventory_aws_ec2.yaml --list 

note: this should display the list of the ec2 instances you have running 

[testing plugin](/images/09_test_plugins_using_ansible-inventory%20command.png)

[output](/images/10_output.png)

Step 8: We will need public dns to enable ansible to connect to the server, my server doesnt have public dns name so i had to go back to my terraform config file and put an attribute called enable_dns-hostname to true in vpc resource 

[public dns](/images/11_we_will_need_public_dns_to_enable_ansible_to_connect_to_the_server_my_servers_dont_have_public_dns_name_so_i_had_to_go_back_to_my_terraform_config_and_put_an_attribute_called_enable_dns_hostnames_to_true_in_vpc_resource.png)

Step 9: Terraform apply 

[terraform apply public dns change](/images/12_terraform_apply.png)
[public dns name on console](/images/13_public_dns_name_on_console.png)

Step 10: In playbook set hosts to group name aws_ec2, you get this group name by default

[host aws_ec2](/images/14_in_playbook_set_hosts_to_group_name_aws_ec2_we_get_this_group_name_by_default.png)

Step 11: Ansible needs user and private key set so you should set this globally in ansible.cfg file 

[user and private key](/images/15_ansible_needs_user_and_private_key_set_so_i_set_this_gloablly_in_ansible_cfg.png)

Step 12: Test playbook 

   ansible-playbook deploy-docker-newuser.yaml

[Testing playbook](/images/16_run_playbook_successful.png)

Step 13: We can also set inventory aws_ec2 as a default param all the  time 

[default inventory](/images/17_we_can_also_set_inventory_aws_ec2_yaml_as_a_default_host_file_so_we_dont_have_to_keep_passing_it_as_a_parameter_all_the_time.png)

Step 14: Create a situation where two dev servers and two prod servers are active

[Two dev two prod](/images/18_creating_a_aituation_where_we_have_two_dev_servers_and_two_prod_servers.png)

[servers on console](/images/19_two_dev_two_prod_servers_on_aws_console.png)

Step 15: If you want to target certain servers there is an attribute in which you can use in plugin file which is keyed_groups, you can then set it to key tags which will look for similar tags and grop them

[keyed groups](/images/20_if_you_want_to_target_certain_servers_there_is_an_attribute_in_which_you_can_use_in_plugin_file_whihc_is_keyed_groups_you_can_then_set_it_to_key_tags_which_will_look_for_similar_tags_and_group_them.png)

Step 16: execute anisble-inventory --grapgh command to see namoes of groups which you can use in your configuration file for hosts

    ansible-inventory -i inventory_aws_ec2.yaml --graph 

[seeing groups based on tags](/images/21_execute_ansible-inventory_--graph_command_to_see_names_of_groups_which_you_can_use_in_your_configuration_file_for_hosts.png)

Step 17: Change hosts in playbook to groupname you want configuration to take place in 

[changing host to specific group name](/images/22_change_hosts_in_playbook_to_groupname_you_want_configuration_to_take_place.png)

[playbook success](/images/23_playbook_cfg_done_on_dev_servers.png)

## Installation

    brew install ansible

## Usage 

    ansible-playbook  deploy-docker-newuser.yaml

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-configure-dynamic-inventory.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-configure-dynamic-inventory

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.